/**
 * <p>Title: liteBatch</p>
 * <p>Description: 一个轻量级，高性能的快速批插工具</p>
 * <p>Copyright: Copyright (c) 2016</p>
 * @author Bryan.Zhang
 * @email 47483522@qq.com
 * @Date 2016-11-10
 * @version 1.0
 */
package com.thebeastshop.batch.handler;

import java.util.List;
import java.util.Observable;
import java.util.concurrent.atomic.AtomicLong;

import com.thebeastshop.batch.callback.ExceptionCallback;
import com.thebeastshop.batch.context.HandlerContext;
import com.thebeastshop.batch.monitor.QueueStatusMonitor;
import com.thebeastshop.batch.queue.RowBatchQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 批插处理器
 */
public abstract class RowBatchHandler<T> extends Observable {

	protected final Logger logger = LoggerFactory.getLogger(RowBatchHandler.class);

	protected AtomicLong loopSize = new AtomicLong(0);

	protected HandlerContext<T> context;

	protected ExceptionCallback<T> exceptionCallback;

	// ========================================

	public RowBatchHandler(HandlerContext<T> context) {
		super();
		// ======添加观察者=====
		this.addObserver(new QueueStatusMonitor<T>(this, context.getMonitorTime()));
		// ===================
		this.context = context;
		this.exceptionCallback = context.getExceptionCallback();
	}

	public abstract void rowBatch(final List<T> batchList);

	public void insertWithBatch(List<T> items) {
		RowBatchQueue<T> queue = context.getQueue();
		if (queue != null && items != null && items.size() > 0) {
			queue.put(items);
			if (!context.isSyn()) {
				loopSize.addAndGet(items.size());
				final long submitCapacity = context.getSubmitCapacity();
				if (loopSize.get() >= submitCapacity) {
					rowBatch(take(submitCapacity));
					loopSize.set(0);
					// 向观察者发送通知
					this.setChanged();
					this.notifyObservers();
				}
			}
		}
	}

	public void flush() {
		rowBatch(takeAll());
	}

	public synchronized List<T> take(long len) {
		try {
			RowBatchQueue<T> queue = context.getQueue();
			if (queue != null) {
				return queue.take(len);
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.error("take is interrupted", e);
			return null;
		}
	}

	public List<T> takeAll() {
		try {
			RowBatchQueue<T> queue = context.getQueue();
			if (queue != null) {
				return queue.takeAll();
			} else {
				return null;
			}
		} catch (Exception e) {
			logger.error("take is interrupted", e);
			return null;
		}
	}
}
